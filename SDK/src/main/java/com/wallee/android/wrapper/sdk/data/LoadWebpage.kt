package com.wallee.android.wrapper.sdk.data

import android.app.Activity
import android.util.Log
import android.webkit.WebView
import android.webkit.WebViewClient

class LoadWebpage {
     var config=Config()

    constructor(context: Activity, webView: WebView, className: String, response: String?,config1: Config) {
        config=config1
        webView.webViewClient = WebViewClient()
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        loadUrl(className, webView)
        webView.addJavascriptInterface(WebPageResponse(context, response,config), config.getKeyWord())
        webView.settings.javaScriptEnabled = true
        webView.settings.setSupportZoom(true)
    }

    private fun loadUrl(className: String, webView: WebView) {

        Log.i("CLASSNAMELoad---->",config.getFinishActivity()+config.getInitialState())
        if (className.equals(config.getInitialState())) {
            webView.loadUrl(config.getstartTransactionurl())
        } else if (className.equals(config.getResponseState())) {
            webView.loadUrl(config.getShowResultUrl())
        }
    }
}