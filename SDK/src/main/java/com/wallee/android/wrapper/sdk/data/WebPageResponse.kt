package com.wallee.android.wrapper.sdk.data

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import android.webkit.JavascriptInterface
import android.widget.Toast
import java.lang.ref.WeakReference

class WebPageResponse(context: Activity?, response: String?,config1: Config) {
    var config = Config()
    var responsenew: String? = response;
    private val mContextRef: WeakReference<Activity?>



    init {
        mContextRef = WeakReference(context)
        config=config1
    }

    @JavascriptInterface
    fun getAndroidVersion(): String? {
        return responsenew
    }

    @JavascriptInterface
    fun showResponse(versionName: String?) {
        Toast.makeText(mContextRef.get(), versionName, Toast.LENGTH_SHORT).show()
    }

    @JavascriptInterface
    fun goBack() {
        val intent = Intent(mContextRef.get(),Class.forName(config.getStartActivity()) )
             mContextRef.get()?.startActivity(intent)
    }

    @JavascriptInterface
    fun startNewTransaction(toastMsg: String?) {
        if (TextUtils.isEmpty(toastMsg) || mContextRef.get() == null) {
            return
        }
        mContextRef.get()!!.runOnUiThread {
            startTransaction(toastMsg);
        }
    }

    fun startTransaction(transactionDetails: String?) {
        var startTrasction: StartTransaction
        startTrasction = StartTransaction(mContextRef.get(),config)
    }
}