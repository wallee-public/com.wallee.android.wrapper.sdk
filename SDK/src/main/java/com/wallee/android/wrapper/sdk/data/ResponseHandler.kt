package com.wallee.android.wrapper.sdk.data

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.wallee.android.till.sdk.ResponseHandler
import com.wallee.android.till.sdk.data.TransactionResponse
import java.io.Serializable
import java.lang.ref.WeakReference


open class ResponseHandler(mContextRef: WeakReference<Activity?>) : ResponseHandler() {
    var config = Config()
    var response: String? = null
    @Override
    fun transactionReply(
        transactionResponse: TransactionResponse?,
        context: Activity?,
        mContextRef: WeakReference<Activity?>,
        config1: Config
    ) {
        config=config1
        if(transactionResponse!= null)
        {
            response = responseToString(transactionResponse)
        }

        Log.i("CLASSNAME---->",config.getFinishActivity()+config.getInitialState())
        var intent = Intent(context, Class.forName("com.example.testwrapperapplication.ResponseActivity"))
        intent.putExtra(config.getKeyWord(),response)
        mContextRef.get()?.startActivity(intent)
    }

    private fun responseToString(transactionResponse: TransactionResponse?): String {
        return "State - " + transactionResponse?.state + "\n" +
                "resultCode code - " + transactionResponse?.resultCode?.code + "\n" +
                "resultCode description - " + transactionResponse?.resultCode?.description + "\n" +
                "amount - " + transactionResponse?.transaction?.totalAmountIncludingTax.toString() + "\n" +
                "authorizationCode - " + transactionResponse?.authorizationCode + "\n" +
                "terminalId - " + transactionResponse?.terminalId + "\n" +
                "sequenceCount - " + transactionResponse?.sequenceCount + "\n" +
                "transactionTime - " + transactionResponse?.transactionTime + "\n" +
                "reserveReference - " + transactionResponse?.reserveReference + "\n" +
                "acquirerId - " + transactionResponse?.acquirerId + "\n" +
                "receipts - " + transactionResponse?.receipts + "\n" +
                "cardIssuingCountry - " + transactionResponse?.cardIssuingCountry + "\n" +
                "cardAppLabel - " + transactionResponse?.cardAppLabel + "\n" +
                "cardAppId - " + transactionResponse?.cardAppId + "\n" +
                "amountTip - " + transactionResponse?.amountTip;
    }

}