package com.wallee.android.wrapper.sdk.data

import android.app.Activity
import android.os.RemoteException
import android.util.Log
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.util.*
import com.wallee.android.till.sdk.ApiClient
import com.wallee.android.till.sdk.data.LineItem
import com.wallee.android.till.sdk.data.Transaction
import com.wallee.android.till.sdk.data.TransactionResponse


class StartTransaction(context: Activity?,config1: Config) {
    private val mContextRef: WeakReference<Activity?>
    private var client: ApiClient? = null
    var config=Config()

    init {
        mContextRef = WeakReference(context)
        config=config1
        val responseHandler: ResponseHandler = object : ResponseHandler(mContextRef) {
            override fun authorizeTransactionReply(response: TransactionResponse?) {
                transactionReply(response, context, mContextRef,config)
            }
        }

        client = ApiClient(responseHandler)
        client?.bind(mContextRef.get())
        val currency: Currency = Currency.getInstance("CHF")
        val amount = BigDecimal("15.00")
        val lineItems = LineItem.ListBuilder("Widget A", amount).build()
        val transaction: Transaction =
            Transaction.Builder(lineItems).setInvoiceReference("IREF-123")
                .setMerchantReference("MREF-123")
                .setCurrency(currency).build()
        try {
            client?.authorizeTransaction(transaction)
        } catch (ex: RemoteException) {
            Log.e("SAMPLE", "API call failed", ex)
        }
    }


}