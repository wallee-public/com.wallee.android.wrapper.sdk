package com.wallee.android.wrapper.sdk.data

class Config {
    var URL_START_TRANSACTION = ""
    var URL_SHOW_RESULT = ""
    var KEY_WORD = ""
    var INITIAL_STATE = ""
    var RESPONSE_STATE = ""
    var START_ACTIVITY=""
    var FINISH_ACTIVITY=""
//Response
    var RESPONSE: String? = null

    fun setResponse(response:String?){
        RESPONSE=response
    }

    fun getResponse():String?{
        return RESPONSE
    }
    fun setstartTransactionurl(url:String){
        URL_START_TRANSACTION=url

    }

    fun getstartTransactionurl():String {
        return URL_START_TRANSACTION
    }

    fun setShowResultUrl(url:String){
        URL_SHOW_RESULT=url

    }

    fun getShowResultUrl():String{
        return URL_SHOW_RESULT
    }

    fun setKeyWord(keyWord:String){
        KEY_WORD=keyWord

    }

    fun getKeyWord():String{
        return KEY_WORD
    }

    fun setInitialState(initialState:String){
        INITIAL_STATE=initialState

    }

    fun getInitialState():String{
        return INITIAL_STATE
    }

    fun setResponseState(responseState:String){
        RESPONSE_STATE=responseState

    }

    fun getResponseState():String{
        return RESPONSE_STATE
    }

    fun setStartActivity(startActivity:String){
        START_ACTIVITY=startActivity
    }
    fun getStartActivity():String{
        return START_ACTIVITY
    }
    fun setFinishActivity(finishActivity:String){
        FINISH_ACTIVITY=finishActivity
    }
    fun getFinishActivity():String{
        return FINISH_ACTIVITY
    }


}